import { createApp } from 'vue'
import { Quasar, Notify } from 'quasar'
import { createPinia } from 'pinia'
import quasarLang from 'quasar/lang/ru'
import {routers} from './routes';

import '@quasar/extras/material-icons/material-icons.css'

import 'quasar/src/css/index.sass'

import App from './App.vue'

const pinia = createPinia()
const myApp = createApp(App)

myApp.use(Quasar, {
    plugins: {
        Notify
    },
    lang: quasarLang,
})
myApp.use(routers)
myApp.use(pinia)

myApp.mount('#app')
