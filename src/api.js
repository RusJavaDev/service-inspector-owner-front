import axios from "axios";
import { useUserStore } from './store/index.js';
import {routers} from './routes.js'

const api = axios.create();

api.interceptors.request.use(config => {
    if (localStorage.getItem('authorization')) {
        config.headers = {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem('authorization')).token}`
        }
    }
    return config;
})

api.interceptors.response.use(config => {
    if (localStorage.getItem('authorization')) {
        config.headers = {
            Authorization: `Bearer ${JSON.parse(localStorage.getItem('authorization')).token}`
        }
    }
    useUserStore().logIn()
    return config;
}, error => {
    console.log(error);
    if (error.response.status === 401) {
        useUserStore().logOut()
        routers.push('/login')
    }
})

export default api;