import {createRouter, createWebHistory} from "vue-router";

const routerHistory = createWebHistory();

export const routers = createRouter({
    history: routerHistory,
    routes: [
        {
            path: '/',
            component: () => import('./pages/checklist_create.vue')
        },
        {
            path: '/checklist-done',
            component: () => import('./pages/checklist_done.vue')
        },
        {
            path: '/tasks',
            component: () => import('./pages/tasks.vue')
        },
        {
            path: '/login',
            component: () => import('./pages/login.vue')
        },
        {
            path: '/checklist-tasks/:id',
            component: () => import('./pages/checklist_tasks.vue')
        },
        {
            path: '/checklist-details/:id',
            component: () => import('./pages/checklist_details.vue')
        },
        {
            path: '/checklist-processing/:id',
            component: () => import('./pages/checklist_processing.vue')
        }
    ]
});