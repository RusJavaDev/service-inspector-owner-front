import {defineStore} from 'pinia'
import {computed, ref} from "vue";


export const useTasksStore = defineStore('tasks', () => {
    const tasksToUpdateByUser = ref([])

    function pushTask(task) {
        tasksToUpdateByUser.value.push(task)
    }

    function findTask(id) {
        return tasksToUpdateByUser.value.find(task => task.id === id)
    }

    function clear() {
        tasksToUpdateByUser.value = []
    }

    const showAllTasks = computed(() => tasksToUpdateByUser.value)

    return {
        tasksToUpdateByUser,
        pushTask,
        findTask,
        clear,
        showAllTasks
    }
})