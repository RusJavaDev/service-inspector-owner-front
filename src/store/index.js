import {defineStore} from 'pinia'
import {computed, ref} from "vue";


export const useUserStore = defineStore('users', () => {
    const isLogged = ref()
    const darkModeEnabled = ref()

    function logIn() {
        isLogged.value = localStorage.getItem('authorization')
    }

    function logOut() {
        isLogged.value = null
        localStorage.clear()
    }

    function toggleDarkMode() {
        darkModeEnabled.value = !darkModeEnabled.value
        return darkModeEnabled.value
    }

    const getIsLogged = computed(() => isLogged.value)

    const getIsDarkModeEnabled = computed(() => darkModeEnabled.value)

    return {
        isLogged,
        logIn,
        logOut,
        getIsLogged,
        darkModeEnabled,
        toggleDarkMode,
        getIsDarkModeEnabled
    }
})